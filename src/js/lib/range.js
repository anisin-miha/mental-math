import $ from 'jquery';

const range = () => {
  $('.range').each((index, item) => {
    let input = $(item).children('input');
    let progress = $(item).find('.range-progress');
    let label = $(item).find('.range-value');
    input.on('input', e => {
      const { value, min, max } = e.target;
      let pos;
      if (min === '0') {
        pos = 100 / max * value;
      } else {
        pos = 100 / (max - 1);
        pos = pos * (value - 1);
      }
      progress.css('width', `${pos}%`);
      label.html(value);
    });
  });
};

export default range;
