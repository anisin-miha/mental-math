import $ from 'jquery';
import { sendResult } from './api';

export const trainer = params => {
  const {
    id,
    operation,
    num_examples,
    amount_numbers,
    digits,
    display_interval,
    examples
  } = params;

  const iterations = examples.length - 1;
  let currentIteration = 0;
  let correctCount = 0;
  let wrongCount = 0;

  function start() {
    const duration = display_interval * 1000;
    drawExamples();
    setTimeout(() => {
      $('.screen.examples').removeClass('active');
      $('.screen.answer').addClass('active');
      $('#answer').focus();
      window.location.hash = 'answer';
    }, examples[currentIteration].numbers.length * duration);
    let index = 0;
    setInterval(() => {
      if (index < examples[currentIteration].numbers.length) {
        $('.blue-2-text')
          .eq(index)
          .removeClass('active');
        $('.blue-2-text')
          .eq(index + 1)
          .addClass('active');
        index++;
      }
    }, duration);
  }

  $(window).keypress(e => {
    if (e.keyCode === 13) {
      runTrainer();
    }
  });

  $('.start-enter').click(() => {
    runTrainer();
  });

  function runTrainer() {
    if (window.location.hash === '#start') {
      window.location.hash = 'operations';
      $('.start-enter').addClass('active');
      setTimeout(() => {
        $('.start-enter').removeClass('active');
        $('.screen.start').removeClass('active');
        $('.screen.examples').addClass('active');
        start();
      }, 100);
    }
  }

  function drawExamples() {
    let text = '';
    examples[currentIteration].numbers.map((item, i) => {
      let active;
      i === 0 ? (active = ' active') : (active = '');
      text += `<p class="blue-2-text${active}">${item}</p>`;
    });
    $('.examples').html(text);
  }

  const answerEl = $('#answer');
  $('#form-answer').submit(e => {
    e.preventDefault();
    const answer = answerEl.val();
    answerEl.val('');
    $('.screen.answer').removeClass('active');
    $('.user-answer').text(answer);
    $('.correct-answer').text(' ' + examples[currentIteration].answer);
    $('.current-progress-bar').val(currentIteration + 1);
    $('.current-progress-bar').attr('max', num_examples);
    $('.current-progress').text(` ${currentIteration + 1} из ${num_examples} `);
    if (parseInt(answer) === parseInt(examples[currentIteration].answer)) {
      $('.screen.correct').addClass('active');
      correctCount++;
    } else {
      $('.screen.wrong').addClass('active');
      wrongCount++;
    }

    setTimeout(() => {
      $('.screen.correct').removeClass('active');
      $('.screen.wrong').removeClass('active');
      if (currentIteration < iterations) {
        currentIteration++;
        $('.screen.examples').addClass('active');
        start();
      } else {
        showResult();
      }
    }, 3000);
  });

  function showResult() {
    window.location.hash = 'result';
    $('.screen.result').addClass('active');
    let operationName = '';
    switch (operation) {
      case '+':
        operationName = 'Сложение';
        break;
      case '-':
        operationName = 'Вычитание';
        break;
      case '*':
        operationName = 'Умножение';
        break;
      case '/':
        operationName = 'Деление';
        break;
      default:
        console.log('error');
    }
    $('#operation-name').text(operationName);
    $('#examples-count').text(num_examples);
    $('#examples-duration').text(display_interval + ' сек');
    $('#examples-correct-count').text(correctCount);
    $('#examples-wrong-count').text(wrongCount);
    $('.result-progress').val(correctCount);
    $('.result-progress').attr('max', num_examples);
    $('#success-progress').text(
      ((100 / num_examples) * correctCount).toFixed(1) + '% '
    );
    sendResult({
      id,
      num_correct_answers: correctCount
    });
  }
};
