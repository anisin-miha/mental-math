import $ from 'jquery';
import { getList, clear } from './api';

const table = () => {
  let btn = $('.clear-table');
  let table = $('.stats__table');

  btn.click(() => {
    table.find('td').remove();
    clear();
  });

  if (window.location.pathname === '/statistics') {
    getList()
      .catch(error => console.log(error))
      .then(({ data }) => {
        let _html = '';
        data.map(({ date, operation, digits, display_interval, result }, i) => {
          _html += `<tr>
          <td>${i+1}.</td>
          <td>${date}</td>
          <td>${operation}</td>
          <td>${display_interval} сек.</td>
          <td>${digits}</td>
          <td>${result}</td>
          </tr>`;
        });
        $('.stats__table tbody').html(_html);
      });
  }
};

export default table;
