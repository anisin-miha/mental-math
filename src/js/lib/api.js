import $ from 'jquery';
import axios from 'axios';

const url = 'http://mmath.herokuapp.com';
const token = localStorage.getItem('token');
const options = {
  headers: { Authorization: `Bearer: ${token}` }
};

export const createExamination = params => {
  return axios.post(url + '/api/examinations/create', params, options);
};

export const loginApi = params => {
  return axios
    .post(url + '/api/login', params)
    .catch(error => {
      let form = $('#login-form');
      let msg = $(form).find('.error-wrap');
      msg.addClass('active');
      form.addClass('error');
      console.log(error);
    })
    .then(({ data: { name, token } }) => {
      localStorage.setItem('name', name);
      localStorage.setItem('token', token);
      window.location.href = '/dashboard';
    });
};

export const registrationApi = params => {
  let form = $('#registration-form');
  let msg = $(form).find('.error-wrap-2');
  let nameInput = $(form).find('.input[name="name"]');
  let emailInput = $(form).find('.input[name="email"]');

  const loginContainer = $('.login__container');
  const regContainer = $('.registration__container');

  let query = axios
    .post(url + '/api/register', params)
    .catch(error => {
      msg.addClass('active');
      nameInput.addClass('error');
      emailInput.addClass('error');
      throw new Error(error);
    })
    .then(res => {
      loginContainer.toggleClass('active');
      regContainer.toggleClass('active');
    });

  return query;
};

export const sendResult = params => {
  return axios.post(url + '/api/examinations/answer', params, options);
};

export const getList = () => {
  return axios.get(url + '/api/examinations/list', options);
};

export const clear = () => {
  return axios.delete(url + '/api/examinations/clear', options);
};
