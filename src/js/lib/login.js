import { loginApi, registrationApi } from './api';

import $ from 'jquery';

const login = () => {
  const loginContainer = $('.login__container');
  const regContainer = $('.registration__container');

  const loginBtn = loginContainer.find('.btn-normal.btn-ghost');
  const regBtn = regContainer.find('.btn-normal.btn-ghost');

  const allInputs = $('.input');

  allInputs.on('input', (e) => {
    allInputs.removeClass('error');
    $('.error-wrap').removeClass('active');
    $('.error-wrap-2').removeClass('active');
  });

  loginBtn.click(() => {
    toggleContainer();
  });

  regBtn.click(() => {
    toggleContainer();
  });

  const toggleContainer = () => {
    loginContainer.toggleClass('active');
    regContainer.toggleClass('active');
  };

  $('#login-form').submit(e => {
    let email = $("input[name='loginEmail']").val();
    let password = $("input[name='loginPassword']").val();
    let form = $('#login-form');
    let msg = $(form).find('.error-wrap');
    e.preventDefault();
    loginApi({ email, password });
  });

  $('#registration-form').submit(e => {
    e.preventDefault();
    let form = e.target;

    let name = $(form)
      .find("[name='name']")
      .val();
    let email = $(form)
      .find("[name='email']")
      .val();
    let password = $(form)
      .find("[name='password']")
      .val();
    let pass2 = $(form)
      .find("[name='password-confirm']")
      .val();
    let legal = $(form).find('input#legal');
    let legalVal = $(form)
      .find('input#legal')
      .is(':checked');

    let text = '';

    let allOk = true;

    let msg = $(form).find('.error-wrap');
    let passwords = $(form).find(".input[type='password']");
    if (password !== pass2) {
      passwords.addClass('error');
      text = 'Пароли не совпадают';
      msg.addClass('active');
      msg.text(text);
      allOk = false;
    }
    if (!legalVal) {
      legal.addClass('error');
      allOk = false;
    }
    if (allOk) {
      $(form).removeClass('error');
      $('.input').removeClass('error');
      passwords.removeClass('error');
      legal.removeClass('error');
      registrationApi({ email, password, name });
    }
  });
};

export default login;
