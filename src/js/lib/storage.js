import $ from 'jquery';

const storage = () => {
  let name = localStorage.getItem('name');
  let token = localStorage.getItem('token');

  if( !isValidJwt(token)) {
    if (window.location.pathname !== '/enter') {
      window.location.href = '/enter';
    }
  }

  if (name) {
    $('.header__user span').text(name);
  }

  let logout = $('.logout');

  logout.click(() => {
    localStorage.clear();
    window.location.href = '/enter';
  });
};

function isValidJwt(jwt) {
  if (!jwt || jwt.split('.').length < 3) {
    return false;
  }
  const data = JSON.parse(atob(jwt.split('.')[1]));
  const exp = new Date(data.exp * 1000); // JS deals with dates in milliseconds since epoch
  const now = new Date();
  return now < exp;
}

export default storage;
