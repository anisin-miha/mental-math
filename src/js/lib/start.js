import $ from 'jquery';
import * as api from './api';
import { trainer } from './trainer';

const start = () => {
  window.location.hash = '';
  const start = $('#start');
  const trainerContainer = $('.blue-content');
  const dashboard = $('.dashboard__container');

  start.click(e => {
    e.preventDefault();
    if (window.location.hash === '') {
      const btnWrap = $(e.target).parent();

      btnWrap.addClass('loading');

      const operation = $('input[name=operation]:checked').data('value');
      const num_examples = $('input[name=num_examples]').val();
      const amount_numbers = $('input[name=amount_numbers]').val();
      const digits = $('input[name=digits]').val();
      const display_interval = $('input[name=display_interval]').val();

      const result = api.createExamination({
        operation,
        num_examples,
        amount_numbers,
        digits,
        display_interval
      });
      result.then(({ data }) => {
        btnWrap.removeClass('loading');
        trainerContainer.addClass('active');
        dashboard.removeClass('active');
        window.location.hash = 'start';
        trainer(data);
      });

      $('#start-again').click(() => {
        trainer(result);
        window.location.hash = 'start';
        $('.screen.result').removeClass('active');
        $('.screen.start').addClass('active');
      });
    }
  });
};

export default start;
