import range from './lib/range';
import login from './lib/login';
import start from './lib/start';
import table from './lib/table';
import storage from './lib/storage';

range();
login();
start();
table();
storage();
